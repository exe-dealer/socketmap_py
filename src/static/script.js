function handleSocketMessage(e) {
    if (e.data instanceof ArrayBuffer) {
        var view = new DataView(e.data);
        var lat = view.getFloat64(0);
        var lng = view.getFloat64(8);
        var userId = view.getInt32(16);
        handleSomeoneMove(userId, lat, lng);
    } else {
        var message = JSON.parse(e.data);
        var eventName = message[0];
        var eventArgs = message.slice(1);
        var handler = serverEventHandlers[eventName];
        handler.apply(this, eventArgs);
    }
}

function handleSomeoneIdent(userId, userName) {
    var marker = getMarkerByUserId(userId);
    marker.setRadius(5 /* px */);
    marker.bindPopup(userName);
}

function handleSomeoneMove(userId, lat, lng) {
    var marker = getMarkerByUserId(userId);
    marker.setLatLng([lat, lng]);
    marker.addTo(map);
}

function handleSomeoneGone(userId) {
    var oldMarker = others[userId];
    delete others[userId];
    oldMarker.fire('remove'); // leaflet 0.7.2 bug
    map.removeLayer(oldMarker);
}

function getMarkerByUserId(userId) {
    return others[userId] || (others[userId] = createMarker());
}

function createMarker() {
    return L.circleMarker(null, {
        color: 'red',
        fillOpacity: 0.8,
        radius: 5 /*px*/
    });
}

function handleSocketOpen() {
    document.body.classList.add('canjoin');
}

function handleSocketClose() {
    location.reload();
}

function handleInitialPosition(position) {
    handleMyPositionChanged(position);
    me.addTo(map);
    me.openPopup();

    navigator.geolocation.watchPosition(
        handleMyPositionChanged,
        handleGeolocationError,
        geolocationOptions
    );
}

function handleMeDrag(e) {
    var pos = me.getLatLng();
    sendMyLatLng(pos.lat, pos.lng);
}

function handleMyPositionChanged(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    me.setLatLng([lat, lng]);
    sendMyLatLng(lat, lng);
}

function sendMyLatLng(lat, lng) {
    var buffer = new ArrayBuffer(16);
    var view = new DataView(buffer);
    view.setFloat64(0, lat);
    view.setFloat64(8, lng);
    sock.send(buffer);
}

function handleGeolocationError(error) {
    switch(error.code) {
    case error.PERMISSION_DENIED:
      alert('User denied the request for Geolocation.');
      break;
    case error.POSITION_UNAVAILABLE:
      alert('Location information is unavailable.');
      break;
    case error.TIMEOUT:
      alert('The request to get user location timed out.');
      break;
    case error.UNKNOWN_ERROR:
      alert('An unknown error occurred.');
      break;
    }
}

function handleJoinClick() {
    navigator.geolocation.getCurrentPosition(
        handleInitialPosition,
        handleGeolocationError,
        geolocationOptions
    );

    document.body.classList.remove('canjoin');
    /*
    handleInitialPosition({
        coords: {
            latitude: leninSatpaev.lat
            longitude: leninSatpaev.lng
        }
    });
    */
}


var serverEventHandlers = {
    'ident': handleSomeoneIdent,
    'gone': handleSomeoneGone
};


var leninSatpaev = L.latLng(43.23890335467301, 76.95884227752686);

var almatyBounds = [
    [43.29519939210697, 76.79821014404297],
    [43.170881894334634, 77.0254898071289]
];






var myNameEl = document.createElement('input');
myNameEl.placeholder = 'Мое имя';
myNameEl.id = 'myname';
myNameEl.onkeyup = function (e) {
    if (e.keyCode === 13) {
        me.closePopup();
    }
};


var popup = L.popup();
popup.setContent(myNameEl);
popup.on('open', myNameEl.focus.bind(myNameEl));
popup.on('close', function () {
    sock.send(myNameEl.value);
});

var me = L.marker(null, { draggable: true });
me.bindPopup(popup);
me.on('drag', handleMeDrag);


var others = {};


var sock = new WebSocket('ws://' + location.host + '/websocket');
sock.binaryType = 'arraybuffer';
sock.onmessage = handleSocketMessage;
sock.onopen = handleSocketOpen;
sock.onclose = handleSocketClose;


var geolocationOptions = {
    enableHighAccuracy: true,
    maximumAge: 5000 // 5 sec.
};


document.getElementById('join').onclick = handleJoinClick;


var map = L.map('map');
map.fitBounds(almatyBounds);

L.tileLayer('http://tile{s}.maps.2gis.com/tiles?x={x}&y={y}&z={z}', {
    subdomains: '0123',
    maxZoom: 18
}).addTo(map);
