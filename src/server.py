import asyncio
import websockets
import json
import logging
import struct

logging.basicConfig(level = logging.INFO)

id_seq = iter(range(2**31))
active_sockets = set()

@asyncio.coroutine
def handle_websocket(curr_socket, uri):
    curr_socket.ident = next(id_seq)
    curr_socket.latest_latlng = None
    curr_socket.username = None

    dump_active = [
        curr_socket.send(json.dumps(
            ('ident', socket.ident, socket.username)
        ))
        for socket in active_sockets
        if socket.username
    ] + [
        curr_socket.send(
            struct.pack('!ddi', *(socket.latest_latlng + (socket.ident,)))
        )
        for socket in active_sockets
        if socket.latest_latlng
    ]

    if dump_active:
        yield from asyncio.wait(dump_active, timeout=0)

    active_sockets.add(curr_socket)

    try:
        while curr_socket.open:
            msg = yield from curr_socket.recv()
            if msg:
                if type(msg) is bytes:
                    # coordinates changed
                    lat, lng = struct.unpack('!dd', msg)
                    lat = max(-90.0, min(90.0, lat))
                    lng = max(-180.0, min(180.0, lng))
                    curr_socket.latest_latlng = lat, lng
                    multicast_event = struct.pack('!ddi', lat, lng, curr_socket.ident)
                else:
                    # username changed
                    curr_socket.username = msg
                    multicast_event = json.dumps((
                        'ident', curr_socket.ident, curr_socket.username
                    ))

                multicast_event_send = [
                    socket.send(multicast_event)
                    for socket in active_sockets
                    if socket != curr_socket
                ]
                if multicast_event_send:
                    yield from asyncio.wait(multicast_event_send, timeout=0)
    finally:
        active_sockets.remove(curr_socket)

        multicast_gone = [
            socket.send(json.dumps(
                ('gone', curr_socket.ident)
            ))
            for socket in active_sockets
            if socket != curr_socket
        ]
        if multicast_gone:
            yield from asyncio.wait(multicast_gone, timeout=0)

start_server = websockets.serve(handle_websocket, '127.0.0.1', 8765)

loop = asyncio.get_event_loop()
loop.run_until_complete(start_server)
loop.run_forever()
