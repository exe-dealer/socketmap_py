import asyncio
import websockets
import struct
import logging
from math import sqrt


logging.basicConfig(level = logging.INFO)


def line_track(lat1, lng1, lat2, lng2):
    step = 0.0001
    length = sqrt( (lng2-lng1)**2 + (lat2-lat1)**2 )
    lng_factor = (lng2-lng1) / length
    lat_factor = (lat2-lat1) / length
    distance = 0
    while distance < length:
        distance += step
        distance = min(distance, length)
        yield (distance*lat_factor + lat1,
               distance*lng_factor + lng1)


def polyline_track(geojson):
    coords = iter(geojson['coordinates'])
    lng1, lat1 = next(coords)
    for lng2, lat2 in coords:
        yield from line_track(lat1, lng1, lat2, lng2)
        lng1, lat1 = lng2, lat2


def loop_track(track, *args):
    while True:
        yield from track(*args)


@asyncio.coroutine
def track_bot(name, track):
    websocket = yield from websockets.connect('ws://localhost:8765/')
    yield from websocket.send(name)

    for lat, lng in track:
        yield from websocket.send(struct.pack('!dd', lat, lng))
        yield from asyncio.sleep(0.5)

    yield from websocket.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait([
    track_bot('bot1', loop_track(polyline_track, {
        "type":"LineString",
        "coordinates":[
            [76.9153743982315,43.2402164279046],
            [76.91602885723114,43.24104099216556],
            [76.9161683320999,43.241123057192326],
            [76.91632390022278,43.24110351791027],
            [76.91640973091124,43.24104490002648],
            [76.91644728183746,43.24094720342821],
            [76.91646337509155,43.24047825757576],
            [76.91643118858337,43.24042354699118],
            [76.91633462905884,43.24040009958277],
            [76.91527247428894,43.2405056128496],
            [76.91507399082184,43.240615033822216],
            [76.91506862640381,43.2407830727904],
            [76.91525638103485,43.240939387693565],
            [76.91552460193634,43.24121684565899],
            [76.91576063632965,43.24142786918107],
            [76.91585183143616,43.241416145671245],
            [76.91596448421478,43.24138488296733],
            [76.916007399559,43.24131454182488],
            [76.91602349281311,43.241244200601216],
            [76.91556215286255,43.24056032336049],
            [76.91518127918242,43.24000930811388],
            [76.91409766674042,43.23816473854477],
            [76.91363096237183,43.23760588547131],
            [76.91300868988037,43.23737140013779],
            [76.91221475601196,43.237136913901935],
            [76.91145300865173,43.23667575500576],
            [76.91043376922607,43.23611298009907],
            [76.90908193588257,43.23598791830287],
            [76.90858840942383,43.239770924087075],
            [76.91534757614136,43.2402164279046]
        ]
    })),

    track_bot('bot2', loop_track(polyline_track, {
        "type":"LineString",
        "coordinates":[
            [76.94829583168028,43.24242826704361],
            [76.95688962936401,43.243076953753196],
            [76.95730805397034,43.239559894826],
            [76.95003390312195,43.23902841048567],
            [76.94877862930298,43.23883691866814],
            [76.94830119609833,43.24242045149898]
        ]
    }))
]))


